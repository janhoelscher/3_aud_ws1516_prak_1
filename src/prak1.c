/*
 * prak1.c
 *
 *  Created on: 25.10.2015
 *      Author: jhr
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "prak1.h"

void menu() {

	printf("Menue zu Warteschlangenverwaltung\n");
	printf("1. Sortiertes Einfuegen eines Elementes\n");
	printf("2. Loeschen und Ausgeben des Elementes mit dem eingegebenen Schluessel\n");
	printf("3. Ausgabe der Liste\n");
	printf("4. Beenden des Programms\n");

	int eingabe;

	do {

		printf("\nBitte geben Sie die Ziffer der gewuenschten Funktion ein: ");
		eingabe = input();

	} while (eingabe <= 0);

	aktion(eingabe);
}

int input() {

	int auswahl = 0;

	scanf("%d", &auswahl);
	fflush(stdin);

	return auswahl;

}

void aktion(int funktion) {

	switch (funktion) {

	case 1:
		printf("\nZahl: ");
		int n = input();
		while (n <= 0) {
			printf("\nUngueltiger Wert. Zahl: ");
			n = input();
		}
		linsert(n);
		printf("\n");
		menu();
		break;

	case 2:
		printf("\nZahl: ");
		int m = input();
		while (m <= 0) {
			printf("\nUngueltiger Wert. Zahl: ");
			m = input();
		}
		ldelete(m);
		printf("\n");
		menu();
		break;

	case 3:
		printList(schlange);
		menu();
		break;

	case 4:
		break;

	default:
		printf(
				"\nSie haben keine der verfuegbaren Funtionen gewaehlt. Versuchen Sie es erneut.\n\n");
		menu();
		break;

	}

}

/* Initialisieren */
void linit() {

	struct Schlange* knoten = malloc(sizeof(struct Schlange));
	knoten->next = NULL;
	knoten->wert = NULL;

	schlange = knoten;

}

/* Hizuf�gen eines elementes */
void linsert(int n) {

	struct Schlange* neuerKnoten = malloc(sizeof(struct Schlange));

	neuerKnoten->wert = n;
	neuerKnoten->next = NULL;

	struct Schlange* tmp = getPos(schlange, n);
	neuerKnoten->next = tmp->next;
	tmp->next = neuerKnoten;

	printf("\nDer Wert %d wurde in die Schlange eingefuegt.\n\n", n);

	//getLast(schlange)->next = neuerKnoten;

}

/* Der Knoten mit dem Wert n soll gelöscht werden */
int ldelete(int n){

    struct Schlange* knoten = schlange->next;
    struct Schlange* lknoten = schlange;

    while(knoten != NULL){

        if(knoten->wert == n)
        break;

        lknoten = knoten;
        knoten = knoten->next;

    }

    if(isempty() == 0 && knoten != NULL && knoten->wert == n){

        lknoten->next = knoten->next;
        int z = knoten->wert;
        free(knoten);

        printf("\nDer Knoten mit dem Wert %d wurde gefunden und entfernt.\n", z);

        return z;

    }else{

        printf("\nDer Knoten mit dem Wert %d wurde nicht gefunden.\n", n);
        return 0;

    }



}

/* Hole den Knoten der Schlange mit Wert <= n */
struct Schlange* getPos(struct Schlange* schlange, int n) {

	struct Schlange* knoten = schlange;

	while (knoten->next != NULL) {

		if (knoten->wert < n && knoten->next->wert >= n)
			break;

		knoten = knoten->next;

	}

	return knoten;

}

/* Hole den letzten Knoten der Schlange */
struct Schlange* getLast(struct Schlange* schlange) {

	struct Schlange* knoten = schlange;

	while (knoten->next != NULL) {

		knoten = knoten->next;

	}

	return knoten;

}

/* Ausgabe der Liste */
void printList(struct Schlange* schlange) {

	struct Schlange* knoten = schlange;
	int i = 1;

	printf("\n\nAusgabe der Liste:\n");

	if (isempty()) {

		printf("Die Schlange ist leer.\n\n");

	} else {

		while (knoten->next != NULL) {

			knoten = knoten->next;
			printf("%d, Wert: %d, Next: %d\n", i, knoten->wert, knoten->next);
			i++;

		}

		printf("\n");

	}

}

/* Anzahl der Knoten in der Liste */
int getSize(struct Schlange* schlange) {

	struct Schlange* knoten = schlange;
	int i = 0;

	while (knoten->next != NULL) {

		knoten = knoten->next;
		i++;

	}

	return i;

}

/* Ob die Schlange leer ist */
int isempty() {

	if (getSize(schlange) < 1) {

		return 1;

	}

	return 0;

}

/* Allokieren */
char* alloc(int n) {

	char* mem = (char*) malloc(n);

	if (mem == NULL) {

		printf("Kein freier Speicher!\n");

		exit(1);

	}

	return mem;
}
