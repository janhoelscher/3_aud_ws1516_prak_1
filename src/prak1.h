/*
 * prak1.h
 *
 *  Created on: 25.10.2015
 *      Author: jhr
 */

#ifndef PRAK1_H_
#define PRAK1_H_

void menu();
int input();
void aktion();

void linit();
void linsert(int n);
int ldelete(int n);

struct Schlange* getPos(struct Schlange* schlange, int n);
struct Schlange* getLast(struct Schlange* schlange);

void printList(struct Schlange* schlange);

int getSize(struct Schlange* schlange);
int isempty();

char* alloc(int n);

struct Schlange {
	int wert;
	struct Schlange* next;
};

struct Schlange* schlange;

#endif /* PRAK1_H_ */
